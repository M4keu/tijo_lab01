package com.company;

public class Triangle extends Figure{
    private int size;

    public Triangle(int size) {
        this.size = size;
    }

    public void draw() {
        for(int i = 0; i <= this.size; i++) {
            for(int j = 0; j < i; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
