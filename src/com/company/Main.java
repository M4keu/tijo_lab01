package com.company;

public class Main {

    public static void printFigure(Figure figure) {
        figure.draw();
    }

    public static void main(String[] args) {
        short age = 33;
        Girl monicaBellucci = new Girl(age);

        monicaBellucci.recieveFlower(new Rose());
        monicaBellucci.getFlower();
        monicaBellucci.age();

        Square square = new Square(5);
        printFigure(square);

        Triangle triangle = new Triangle(5);
        printFigure(triangle);

        Circle circle = new Circle();
        printFigure(circle);
    }
}
