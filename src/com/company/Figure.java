package com.company;

abstract class Figure {
    abstract void draw();
}
