package com.company;

public class Square extends Figure {
    private int size;

    public Square(int size) {
        this.size = size;
    }

    public void draw() {
        for(int i = 0; i < this.size; i++) {
            for(int j = 0; j < this.size; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
